#include "StaticLib.h"

namespace StaticLibrary
{

	void WriteSomething()
	{
		std::cout << "Writing from a static library\n";
	}
}