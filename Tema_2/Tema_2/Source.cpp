
#include<StaticLib.h>
#include<DynamicLib1.h>
#include<Windows.h>

typedef void(__cdecl *OnRunProcedure)();
extern "C" void __cdecl	Write();

void main()
{
	StaticLibrary::WriteSomething();	//static lib function
	Write();	//Load-Time dll function
	HINSTANCE hinstLib = LoadLibrary(TEXT("Exercise1_DynamicLibrary2.dll"));
	OnRunProcedure procedure = (OnRunProcedure)GetProcAddress(hinstLib, "Write");
	if (NULL != procedure)
		procedure();	//Run-Time dll function
}