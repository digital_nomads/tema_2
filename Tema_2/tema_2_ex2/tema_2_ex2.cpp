#include<iostream>
#include<vector>

int OutOfBoundsError(std::vector<int> array);	
void SyntaxError(std::vector<int> array);	
void UninitializedPointerError();



void main()
{
	std::vector<int> myArray = {1, 15, 100, 0, 13, 2, 5, 3};
	std::cout<<OutOfBoundsError(myArray);
	SyntaxError(myArray);
	UninitializedPointerError();
}

int OutOfBoundsError(std::vector<int> array)
{
	int highestValue = INT_MIN;
	for (int i = 0; i < array.size(); ++i)	//fixed
	{
		if (array[i] > highestValue)
			highestValue = array[i];
	}
	return highestValue;
}

void SyntaxError(std::vector<int> array)
{
	bool isNotSorted = true;
	while (isNotSorted)	//fixed
	{
		isNotSorted = false;
		for (int i = 0; i < array.size() - 1; ++i)
		{
			if (array[i] > array[i + 1])
			{
				isNotSorted = true;
				int aux = array[i];
				array[i] = array[i + 1];
				array[i + 1] = aux;
			}
		}
	}
}

void UninitializedPointerError()
{
	int *pointer = new int;	//fixed
	*pointer = rand();
}
