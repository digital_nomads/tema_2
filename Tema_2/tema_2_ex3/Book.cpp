
#include "Book.h"
#include <fstream>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <sstream>
#include "defines.h"

/*
* Constructor
*/
Book::Book()
{}

/*
* Destructor
*/
Book::~Book()
{}

/*
* Constructor that creates the map
*/
Book::Book(std::string filePath)
{
   construct(filePath); 
}
/*
* Sets the map with each word and its count
*/
void Book::construct(std::string filePath)
{
   START_TIME;

   std::ifstream file;
   file.open(filePath);
   std::string word;

   while (file >> word) {
      wordTrim(word);
      _content[word]++;
   }
   STOP_TIME;
   _constructTime = ELAPSED_TIME;
   file.close();
}
/*
* returns the time it takes to construct the map in microseconds
*/
long long Book::getConstructTime()
{
   return _constructTime;
}

/*
* returns the time it takes to check if a word exists
*/
long long Book::getQueryTime()
{
   return _queryTime;
}
/*
* returns the time it takes to find the 3 most frequent words
*/
long long Book::getFindCommonWordsTime()
{
   return _findCommonWordsTime;
}
/*
* returns the time it takes to check the number of times a word appears
*/
long long Book::getFindWordCountTime()
{
   return _findWordCountTime;
}
/*
* returns true if the words exists false otherwise
*/
bool Book::existsWord(std::string word)
{
   START_TIME;
   bool exists=(_content.find(word) == _content.end()) ? false : true;
   STOP_TIME;
   _queryTime = ELAPSED_TIME;

   return exists;
}
/*
* returns the number of times a word appears 
*/
int Book::countWord(std::string word)
{
   START_TIME;
   int nr = _content[word];
   STOP_TIME;
   _findWordCountTime = ELAPSED_TIME;
   return nr;
}
/*
* returns the most common 3 words
* each pair contains the word and the count
*/
std::vector<std::pair<std::string,int>> Book::commonWords()
{
   std::string firstWord;
   std::string secondWord;
   std::string thirdWord;

   int firstNr=0;
   int secondNr = 0;
   int thirdNr = 0;
   START_TIME;
   for (auto word:_content) {
      if (word.second > firstNr) {
         thirdNr = secondNr;
         secondNr = firstNr;
         firstNr = word.second;

         thirdWord = secondWord;
         secondWord = firstWord;
         firstWord = word.first;
      }else if(word.second>secondNr){
         thirdNr = secondNr;
         secondNr = word.second;

         thirdWord = secondWord;
         secondWord = word.first;
      } else if (word.second > thirdNr) {
         thirdNr = word.second;

         thirdWord = word.first;
      }
   }
   STOP_TIME;
   _findCommonWordsTime = ELAPSED_TIME;
   return std::vector<std::pair<std::string, int>>{ {firstWord, firstNr}, {secondWord,secondNr}, {thirdWord,thirdNr}};
}
/*
* helper function used to get rid of unneeded characters and capitalization
* Ex: Hello!? -> hello
*/
void Book::wordTrim(std::string& word)
{
   char token[] = ".-/?!,;'()_\"";
   while (strchr(token, word[0]) != nullptr) {
      word.erase(0, 1);
   }
   while (strchr(token, word[word.length() - 1]) != nullptr) {
      word.erase(word.length() - 1, 1);
   }
   std::transform(word.begin(), word.end(), word.begin(), ::tolower);
}
/*
* returns a string with all the measurings for the homework
*/
std::string Book::taskMeasuring()
{
   std::stringstream result;
   result<< "The most common words: ";
   std::vector<std::pair<std::string, int>> commonWords = this->commonWords();
   result << commonWords[0].first << ":" << commonWords[0].second<<" ";
   result << commonWords[1].first << ":" << commonWords[1].second<<" ";
   result << commonWords[2].first << ":" << commonWords[2].second<<" \n";
   
   result << "Time for constructing:";
   result << _constructTime << " microseconds\n";

   this->existsWord("washington");
   result << "Time for finding a word:";
   result << _queryTime << " microseconds\n";

   this->countWord("washington");
   result << "Time for finding the number of times a word apears:";
   result << _findWordCountTime << " microseconds\n\n";

   return result.str();
}