#pragma once
#include <vector>
#include <map>
#include <string>
#include <set>
#include <chrono>

class Book
{

protected:
   std::map<std::string, int> _content;

   long long _constructTime;
   long long _queryTime;
   long long _findCommonWordsTime;
   long long _findWordCountTime;

   void wordTrim(std::string & word);
   virtual void construct(std::string filePath);

public:
   Book();
   Book(std::string filePAth);
   ~Book();

   long long getConstructTime();
   long long getQueryTime();
   long long getFindCommonWordsTime();

   long long getFindWordCountTime();

   bool existsWord(std::string word);
   int countWord(std::string word);
   virtual std::vector<std::pair<std::string, int>> commonWords();
   virtual std::string taskMeasuring();
};

