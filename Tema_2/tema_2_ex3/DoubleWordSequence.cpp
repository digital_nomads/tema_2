
#include "DoubleWordSequence.h"
#include <fstream>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <sstream>
#include "defines.h"

/*
* constructor
*/
DoubleWordSequence::DoubleWordSequence()
{}

/*
* constructor that calls the construct function
*/
DoubleWordSequence::DoubleWordSequence(std::string filePath)
{
   construct(filePath);
}

/*
* destructor
*/
DoubleWordSequence::~DoubleWordSequence()
{}

/*
* sets the map with 2 word sequences 
*/
void DoubleWordSequence::construct(std::string filePath)
{
   std::ifstream file;
   file.open(filePath);
   std::string firstWord;
   std::string secondWord;

   START_TIME;

   file >> firstWord;
   wordTrim(firstWord);


   while (file >> secondWord) {
      wordTrim(secondWord);

      _content[firstWord + " " + secondWord]++;

      firstWord = secondWord;
   }
   STOP_TIME;
   _constructTime = ELAPSED_TIME;
   file.close();
}

/*
* returns the most common sequence of words
* the returned pair contains the word and the number of times it appears
*/
std::pair<std::string, int> DoubleWordSequence::commonSequence()
{
   std::string sequence;
   int nr = 0;
   START_TIME;

   for (auto item : _content) {
      if (item.second > nr) {
         nr = item.second;
         sequence = item.first;
      }
   }
   STOP_TIME;
   _findCommonWordsTime = ELAPSED_TIME;

   return std::pair<std::string, int>{sequence, nr};
}

/*
* returns a string with all the measurings for the homework
*/
std::string DoubleWordSequence::taskMeasuring()
{
   std::stringstream result;
   result << "The most common  word sequence: ";
   std::pair<std::string, int> sequence = commonSequence();
   result << sequence.first << ":" << sequence.second<<"\n";

   result << "Time for finding: " << _constructTime << "microseconds\n\n";


   return result.str();
}
