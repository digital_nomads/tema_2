#pragma once
#include "Book.h"
class DoubleWordSequence : public Book
{
public:
   DoubleWordSequence();
   DoubleWordSequence(std::string filePath);
   ~DoubleWordSequence();

   virtual void construct(std::string filePath) override;
   virtual std::pair<std::string, int> commonSequence();
   virtual std::string taskMeasuring() override;
};

