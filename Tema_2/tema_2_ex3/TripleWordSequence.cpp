
#include "TripleWordSequence.h"
#include <fstream>
#include <cstring>
#include <algorithm>
#include <iostream>
#include "defines.h"

/*
* Constructor that creates the map
*/
TripleWordSequence::TripleWordSequence(std::string filePath)
{
   construct(filePath);
}

/*
* constructor
*/
TripleWordSequence::TripleWordSequence()
{}

/*
* destructor
*/
TripleWordSequence::~TripleWordSequence()
{}
/*
* sets the map with 2 word sequences
*/
void TripleWordSequence::construct(std::string filePath)
{
   std::ifstream file;
   file.open(filePath);
   std::string firstWord;
   std::string secondWord;
   std::string thirdWord;

   START_TIME;

   file >> firstWord;
   wordTrim(firstWord);
   file >> secondWord;
   wordTrim(secondWord);

   while (file >> thirdWord) {
      wordTrim(thirdWord);

      _content[firstWord + " " + secondWord + " " + thirdWord]++;

      firstWord = secondWord;
      secondWord = thirdWord;
   }
   STOP_TIME;
   _constructTime = ELAPSED_TIME;
   file.close();
}
