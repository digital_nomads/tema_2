#pragma once
#include "DoubleWordSequence.h"

class TripleWordSequence :public DoubleWordSequence
{
public:
   TripleWordSequence(std::string filePath);
   TripleWordSequence();
   ~TripleWordSequence();

   virtual void construct(std::string filePath) override;
};

