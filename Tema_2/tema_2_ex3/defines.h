#pragma once


#define START_TIME std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now()
#define STOP_TIME  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now()  
#define ELAPSED_TIME std::chrono::duration_cast<std::chrono::microseconds> (end - begin).count()
