// tema_2_ex3.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <map>
#include <string>

#include "Book.h"
#include "DoubleWordSequence.h"
#include "TripleWordSequence.h"

int main()
{
   Book book("text.txt");
   std::cout << book.taskMeasuring();

   DoubleWordSequence d("text.txt");
   std::cout << d.taskMeasuring();

   TripleWordSequence t("text.txt");
   std::cout << t.taskMeasuring();

   return 0;
}



